{ config, pkgs, ... }:

let
    ipv4Prefix = "10.0.42";
    ipv4Address = "${ipv4Prefix}.3";
    gatewayIPv4 = "${ipv4Prefix}.1";

    ipv6Prefix = "2403:580b:7fff:7a00";
    ipv6Address = "${ipv6Prefix}::a423"; 
in {
    networking = {
        defaultGateway = gatewayIPv4;
        nameservers = [ gatewayIPv4 ];

        interfaces = {
            enabcm6e4ei0 = {
                useDHCP = false;
                ipv4.addresses = [ 
                    {
                        address = ipv4Address;
                        prefixLength = 24;
                    }
                ];
                ipv6.addresses = [
                    {
                        address = ipv6Address;
                        prefixLength = 24;
                    }
                ];
            };
        };
        hosts = 
            let
                ownDomains = [
                    "jenkins.lan.paroz.io"
                    "pi.hole"
                    "pi.lan.paroz.io"
                    "ry-pi"
                    "syncthing.lan.paroz.io"
                    "web.lan.paroz.io"
                ];
            in {
                ${ipv4Address} = ownDomains;
                ${ipv6Address} = ownDomains;
                ${gatewayIPv4} = [ "router.lan.paroz.io" ];
            };
    };

    services.ry-pi = {
        repoPath = "/home/ryan/ry-pi";
        ipv6Prefix = ipv6Prefix;
        piHole = {
            ipv4Address = ipv4Address;
            ipv6Address = ipv6Address;
            upstreamDnsServers = [ "${gatewayIPv4}#53" ];
            dhcp.range = { start = "${ipv4Prefix}.32"; end = "${ipv4Prefix}.223"; };
        };
    };

    virtualisation.docker = {
        extraOptions = "--fixed-cidr-v6 ${ipv6Prefix}:d0::/72";
    };
}
