# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, ... }:

let
    requiredImports = [   
        ./hardware-configuration.nix
        ./ry-pi-services.nix
    ];
    optionalImports = [
        ./configuration.local.nix
    ];
in {
    imports = builtins.concatLists [
        requiredImports
        (builtins.filter (lib.sources.pathIsRegularFile) optionalImports)
    ];

    fileSystems = {
        "/mnt/ssd" = {
            device = "/dev/disk/by-label/RY-PI_STORE";
            fsType = "ext4";
        };
    };

    # Use the systemd-boot EFI boot loader.
    boot.loader = {
        timeout = 3;
        systemd-boot.enable = true;
        efi.canTouchEfiVariables = true;
    };

    boot.initrd.availableKernelModules = [ "usbhid" "usb_storage" ];

    # Set your time zone.
    time.timeZone = "Australia/Brisbane";

    networking = {
        hostName = "ry-pi";

        # The global useDHCP flag is deprecated, therefore explicitly set to false here.
        # Per-interface useDHCP will be mandatory in the future, so this generated config
        # replicates the default behaviour.
        useDHCP = false;
        interfaces = {
            # Ideally this should be configured to false in local config, with static IP settings.
            #   If this is not done, DHCPCD will cause a few seconds of lag on the SSH session every minute.
            wlan0.useDHCP = false;
        };
    };

    # Select internationalisation properties.
    i18n.defaultLocale = "en_AU.UTF-8";
    console = {
        font = "Lat2-Terminus16";
        keyMap = "us";
    };


    users.mutableUsers = false;
    users.users.ryan = {
        isNormalUser = true;
        uid = 1000;
        name = "ryan";
        hashedPassword = "$6$KEP7j4vlza90EFSJ$HU/b.Qtph1y2L7I1l0xiEURoelF6CkJpG/gMNhQMvQdhXnJrtLsULHFSyT2ofyUosfQXfYwAFvsR9Resb7t/x/";

        home = "/home/ryan";
        createHome = true;

        shell = pkgs.zsh;
        extraGroups = [ "docker" "video" "wheel" ]; # Enable ‘sudo’ for the user.
    };

    environment.systemPackages = with pkgs; [
        docker-compose
        fio
        git
        inetutils
        libcgroup
        libraspberrypi
        mkpasswd
        python3Full
        wget
        vim
    ];

    programs.tmux.enable = true;
    programs.zsh = {
        enable = true;
        syntaxHighlighting.enable = true;
    };

    # List services that you want to enable:

    services = {
        jenkins = {
            enable = true;
            port = 8080;
        };
        # Enable the OpenSSH daemon.
        openssh = {
            enable = true;
            passwordAuthentication = true;
        };
        ry-pi = {
            enable = true;
            projectName = "ry-pi";
            dataPath = "/mnt/ssd/ryan";
            piHole = {
                adminEmail = "ryan@paroz.io";
                domain = "lan.paroz.io";
                webTheme = "default-dark";
                dhcp = {
                    enable = true;
                    enableIPv6 = true;
                    leaseTime = 24;
                };
            };
        };
        syncthing = {
            enable = true;
            guiAddress = "0.0.0.0:8384";
            dataDir = "/mnt/ssd/ryan/syncthing";
            configDir = "/mnt/ssd/ryan/syncthing/.config/syncthing";
            devices = import ./syncthing/devices.nix;
            folders = import ./syncthing/folders.nix;
            overrideDevices = true;
            overrideFolders = true;
        };
        udev = {
            extraRules = "KERNEL==\"vchiq\", OWNER=\"root\", GROUP=\"video\"";
        };
    };

    virtualisation.docker = {
        enable = true;
        extraOptions = "--bridge docker0 --default-address-pool base=172.100.0.0/16,size=24 --ipv6";
    };

    # Open ports in the firewall.
    networking.firewall.allowedTCPPorts = [ 22 80 443 2222 8080 8384 22000 21027 25565 ];
    networking.firewall.allowedUDPPorts = [ 25565 ];

    nix = {
        autoOptimiseStore = true;
        gc = {
            automatic = true;
            dates = "weekly";
            options = "--delete-older-than 30d";
        };
        # Free up to 1GiB whenever there is less than 100MiB left.
        extraOptions = ''
            min-free = ${toString (100 * 1024 * 1024)}
            max-free = ${toString (1024 * 1024 * 1024)}
        '';
    };

    nixpkgs.config.allowUnfree = true;

    powerManagement.cpuFreqGovernor = "ondemand";

    # This value determines the NixOS release from which the default
    # settings for stateful data, like file locations and database versions
    # on your system were taken. It‘s perfectly fine and recommended to leave
    # this value at the release version of the first install of this system.
    # Before changing this value read the documentation for this option
    # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
    system.stateVersion = "21.11"; # Did you read the comment?

}
