{ config, lib, pkgs,... }:

with lib;

let
    cfg = config.services.ry-pi;

    serviceScriptPath = with pkgs; [
        docker
        docker-compose
    ];
    withEnvironmentSetup = {body, additionalVars ? {}}:
        let
            makeExport = name: value: "export ${name}=\"${builtins.toString value}\"";
            makeExports = vars: attrsets.mapAttrsToList (makeExport) vars;
            makeExportStr = vars: strings.concatStringsSep "\n" (makeExports vars);

            commonExports = makeExportStr {
                PROJECT_NAME = cfg.projectName;
                DATA_DIRECTORY = cfg.dataPath;
                IPV6_PREFIX_64 = cfg.ipv6Prefix;
                compose_file = "${cfg.repoPath}/src/services/compose.yaml";
            };
        in ''
            ${commonExports}
            ${makeExportStr additionalVars}

            ${body}
        '';
in
 {
    imports = [];

    options.services.ry-pi = {
        enable = mkOption {
            type = types.bool;
            default = true;
            description = ''
                Enable docker-based services for the Ry-Pi device.
            '';
        };
        projectName = mkOption {
            type = types.str;
            description = ''
                The name to use for the docker-compose project running the services.
            '';
        };
        repoPath = mkOption {
            type = types.str;
            description = ''
                The path to the cloned ry-pi repository (where this .nix file originally came from).

                (TODO: Let's just make a package for this and clone the repo when building said package.)
            '';
        };
        dataPath = mkOption {
            type = types.str;
            description = ''
                The path where data for the docker services should be stored (ideally with faster I/O).
            '';
        };
        ipv6Prefix = mkOption {
            type = types.str;
            description = ''
                The 64-bit IPv6 prefix that may be used to create IP addresses for the docker containers. (Subnet is currently hardcoded to :1142:)
            '';
        };
        piHole =
            let
                piHoleOpts = {
                    options = {
                        adminEmail = mkOption {
                            type = types.str;
                            default = "ryan@paroz.io";
                            description = ''
                                The admin email for Pi-Hole to use.
                            '';
                        };
                        hostName = mkOption {
                            type = types.str;
                            default = config.networking.hostName;
                            description = ''
                                The hostname of the device the Pi-Hole lives on.
                            '';
                        };
                        ipv4Address = mkOption {
                            type = types.str;
                            description = ''
                                The IPv4 address for the Pi-Hole to associate with.
                            '';
                        };
                        ipv6Address = mkOption {
                            type = types.nullOr types.str;
                            default = null;
                            description = ''
                                The IPv6 address for the Pi-Hole to associate with.
                            '';
                        };
                        domain = mkOption {
                            type = types.nullOr types.str;
                            default = null;
                            description = ''
                                The domain name for Pi-Hole to associate with.
                            '';
                        };
                        routerIPv4Address = mkOption {
                            type = types.str;
                            default = config.networking.defaultGateway.address;
                            description = ''
                                The IPv4 address of the default router for the network the pi-hole is on.
                            '';
                        };
                        upstreamDnsServers = mkOption {
                            type = types.listOf types.str;
                            default = [ ];
                            apply = lib.unique;
                            description = ''
                                A list of DNS servers to use for upstream name resolution.
                            '';
                        };
                        webTheme = mkOption {
                            type = types.enum [ "default-dark" "default-darker" "default-light" "default-auto" "lcars" ];
                            default = "default-light";
                            description = ''
                                User interface theme for Pi-Hole to use.
                            '';
                        };
                        dhcp =
                            let dhcpOpts = {
                                    options = {
                                        enable = mkOption {
                                            type = types.bool;
                                            default = false;
                                            description = ''
                                                Enable DHCP for this Pi-Hole.
                                            '';
                                        };
                                        enableIPv6 = mkOption {
                                            type = types.bool;
                                            default = false;
                                            description = ''
                                                Enable DHCPv6 for this Pi-Hole.
                                            '';
                                        };
                                        leaseTime = mkOption {
                                            type = types.int;
                                            default = 24;
                                            description = ''
                                                Lease time (in hours) for DHCP addresses.
                                            '';
                                        };
                                        range =
                                            let
                                                defaultStart = "192.168.1.32";
                                                defaultEnd = "192.168.1.192";
                                                rangeOpts = {
                                                    options = {
                                                        start = mkOption {
                                                            type = types.str;
                                                            default = defaultStart;
                                                            description = ''
                                                                Start of the range of IPv4 addresses to lease.
                                                            '';
                                                        };
                                                        end = mkOption {
                                                            type = types.str;
                                                            default = defaultEnd;
                                                            description = ''
                                                                End of the range of IPv4 addresses to lease.
                                                            '';
                                                        };
                                                    };
                                                };
                                            in mkOption {
                                                type = types.submodule rangeOpts;
                                                default = { start = defaultStart; end = defaultEnd; };
                                                description = ''
                                                    The range of addresses for DHCP to lease.
                                                '';
                                            };
                                    };
                                };
                            in mkOption {
                                type = types.submodule dhcpOpts;
                                description = ''
                                    DHCP options for Pi-Hole.
                                '';
                            };
                    };
                };
            in mkOption {
                type = types.submodule piHoleOpts;
                description = ''
                    Options for the Pi-Hole service.
                '';
            };
    };

    config = {
        systemd.services = {
            ry-pi-build = {
                description = "Build services necessary for a Ry-Pi environment";
                path = serviceScriptPath;

                before = [ "ry-pi.service" ];
                wantedBy = [ "multi-user.target" ];

                serviceConfig = {
                    Type = "oneshot";
                    RemainAfterExit = true;
                };

                script =
                    let buildSrc = builtins.readFile "${cfg.repoPath}/src/services/lib/do_build.sh";
                    in withEnvironmentSetup { body = buildSrc; };
            };
            ry-pi = mkIf cfg.enable {
                description = "Run Ry-Pi services";
                path = serviceScriptPath;

                after = [ "ry-pi-build.service" ];
                requires = [ "ry-pi-build.service" ];
                wantedBy = [ "multi-user.target" ];

                script =
                    let
                        ph = cfg.piHole;

                        runSrc = builtins.readFile "${cfg.repoPath}/src/services/lib/do_run.sh";
                    in withEnvironmentSetup {
                        body = runSrc;
                        additionalVars = {
                            ADMIN_EMAIL = ph.adminEmail;
                            DHCP_ACTIVE = if ph.dhcp.enable then "true" else "false";
                            DHCP_IPV6 = if ph.dhcp.enableIPv6 then "true" else "false";
                            DHCP_LEASETIME = builtins.toString ph.dhcp.leaseTime;
                            DHCP_START = ph.dhcp.range.start;
                            DHCP_END = ph.dhcp.range.end;
                            DHCP_ROUTER =
                                if builtins.isNull ph.routerIPv4Address
                                then ""
                                else ph.routerIPv4Address;
                            FTLCONF_LOCAL_ADDR4 = ph.ipv4Address;
                            FTLCONF_LOCAL_ADDR6 = if builtins.isNull ph.ipv6Address then "" else ph.ipv6Address;
                            PIHOLE_DNS_ = strings.concatStringsSep ";" ph.upstreamDnsServers;
                            PIHOLE_DOMAIN = if builtins.isNull ph.domain then "" else ph.domain;
                            PIHOLE_HOSTNAME = ph.hostName;
                            PIHOLE_WEB_THEME = ph.webTheme;
                        };
                    };
            };
        };
        networking.firewall = {
            allowedTCPPorts = [ 53 80 547 ];
            allowedUDPPorts = [ 53 67 68 547 ];
            trustedInterfaces = [ "br-http-services" ];
        };
    };
}
