if [[ -z "$compose_file" ]]; then
    compose_file="$(pwd)/compose.yaml"
fi

if [[ -z "${PROJECT_NAME}" ]]; then
    PROJECT_NAME="services"
fi

echo "Building services for project ${PROJECT_NAME}..."
docker-compose --file "$compose_file" --project-name "${PROJECT_NAME}" build
echo "${PROJECT_NAME} built!"
