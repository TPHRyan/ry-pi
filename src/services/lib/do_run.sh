if [[ -z "$compose_file" ]]; then
    compose_file="$(pwd)/compose.yaml"
fi

if [[ -z "${PROJECT_NAME}" ]]; then
    PROJECT_NAME="ry-pi"
fi

echo "Running project as \"${PROJECT_NAME}\"."
exec docker-compose --file "$compose_file" --project-name "${PROJECT_NAME}" up
