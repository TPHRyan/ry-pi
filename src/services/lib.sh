#!/usr/bin/env bash

source .containers

load_environment() {
    if [[ ! -f "../../env/.env" ]]; then
        echo ".env file not found."
        echo "Please copy .dist.env file(s) to .env files and fill in values!"
        exit 1
    fi
    source .env

    for container in "${CONTAINERS[@]}"; do
        container_file=".${container}.env"
        if [[ -f "$container_file" ]]; then
            source "../../env/$container_file"
        fi
    done
}
