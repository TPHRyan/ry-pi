#!/usr/bin/env bash

SCRIPT_FILENAME="$(realpath "$0")"
SCRIPT_DIRECTORY="$(dirname "$SCRIPT_FILENAME")"

cd "$SCRIPT_DIRECTORY"

source ./lib.sh
load_environment

compose_file="$(pwd)/compose.yaml"
export compose_file
source ./lib/do_run.sh
