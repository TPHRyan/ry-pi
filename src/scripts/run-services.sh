#!/usr/bin/env bash

SCRIPT_FILENAME="$(realpath "$0")"
SCRIPT_DIRECTORY="$(dirname "$SCRIPT_FILENAME")"

src="$(dirname "$SCRIPT_DIRECTORY")"

exec /usr/bin/env bash "$src/services/run.sh"
