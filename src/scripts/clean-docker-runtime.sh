#!/usr/bin/env bash

SCRIPT_FILENAME="$(realpath "$0")"
SCRIPT_DIRECTORY="$(dirname "$SCRIPT_FILENAME")"

cd "$SCRIPT_DIRECTORY"

remove_file () {
    file_glob="$1"
    removing_what="$2"
    if [[ -z "$removing_what" ]]; then
        removing_what="$file_glob"
    fi
    if [[ -f "$file_glob" ]]; then
        echo "Removing ${removing_what} file..."
        rm "$file_glob"
    else
        echo "${file_glob} does not exist, skipping..."
    fi
}

remove_directory () {
    dir_glob="$1"
    removing_what="$2"
    if [[ -z "$removing_what" ]]; then
        removing_what="$dir_glob"
    fi
    if [[ -d "$dir_glob" ]]; then
        echo "Removing ${removing_what} directory..."
        rm -r "$dir_glob"
    else
        echo "${dir_glob} does not exist, skipping..."
    fi
}


echo "NOTICE: This script should be run as root, and is intended to run prior to the docker.service unit!"

remove_directory "/var/run/docker"
remove_file "/var/run/docker.sock" "docker.sock"
remove_directory "/var/lib/docker/containers/*"
remove_directory "/var/lib/docker/network/*"
