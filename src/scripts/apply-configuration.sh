#!/usr/bin/env bash

SCRIPT_FILENAME="$(realpath "$0")"
SCRIPT_DIRECTORY="$(dirname "$SCRIPT_FILENAME")"

src="$(dirname "$SCRIPT_DIRECTORY")"

echo "Requesting sudo access to copy the configuration into /etc/nixos, then run nixos-rebuild on your behalf."
sudo cp -Riu "$src/nixos"/* /etc/nixos/
sudo nixos-rebuild switch
