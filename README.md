# ry-pi

My home Raspberry Pi, run using containers and additional magic.

# Manual Configuration Steps

Because I do not have infinite time, these are things that should manually be done after cloning currently:

- In ./src/services, copy all .dist.env files to .env files and fill all the variables that need to change.
- Create `pihole_web_password.txt` and set the password for the Pi Hole web portal
